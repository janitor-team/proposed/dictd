Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact:
 Aleksey Cheusov <vle@gmx.net>
 Rickard E. Faith <faith@dict.org>
Source:
 http://sourceforge.net/projects/dict/
Files-Excluded:
 doc/rfc2229.txt
 doc/rfc.ms
 doc/toc.ms


Files: *
Copyright:
 Copyright (C) 1994-2003 Rickard E. Faith (faith@dict.org)
 Copyright (C) 2002-2008 Aleksey Cheusov (vle@gmx.net)
License: GPL-1+ and GPL-2+


Files: md5.c
Copyright:
  written by Colin Plumb in 1993, no copyright is claimed
License: public-domain
 This code is in the public domain; do with it what you wish


Files: strlcat.c strlcpy.c
Copyright:
 Copyright (c) 1998 Todd C. Miller <Todd.Miller@courtesan.com>
 Copyright (C) 2001 Richard Kettlewell <rjk@greenend.org.uk>
License: BSD-3-clause


Files: dict_lookup
Copyright:
 Copyright (c) 2010  Alexander Vorontsov <vorontsov@imb.invention-machine.com>
 Copyright (c) 2010  Aleksey Cheusov     <vle@gmx.net>
License: dict_lookup_license
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Files: dictzip.1.in dict.1.in dictd.8.in dictfmt.1.in
Copyright:
 Copyright 1997, 1998 Rickard E. Faith (faith@acm.org)
 Copyright 2002-2008 Aleksey Cheusov (vle@gmx.net)
 Copyright 2000 Robert D. Hilliard <hilliard@debian.org>
License: dict_manpages_license
 Permission is granted to make and distribute verbatim copies of this
 manual provided the copyright notice and this permission notice are
 preserved on all copies.
 .
 Permission is granted to copy and distribute modified versions of this
 manual under the conditions for verbatim copying, provided that the
 entire resulting derived work is distributed under the terms of a
 permission notice identical to this one


Files: debian/*
Copyright:
 Copyright (C) 1998-2004 Robert D. Hilliard <hilliard@debian.org>
 Copyright (C) 2004-2007 Kirk Hilliard <kirk@debian.org>
 Copyright (C) 2007      Julien BLACHE <jblache@debian.org>
 Copyright (C) 2007      Pierre Habouzit <madcoder@debian.org>
 Copyright (C) 2008-2020 Robert Luberda <robert@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 Licenses can be found in the file /usr/share/common-licenses/GPL-2

License: GPL-1+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 1, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 Licenses can be found in the file /usr/share/common-licenses/GPL-1

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
   3. The name of the author may not be used to endorse or promote products
      derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
